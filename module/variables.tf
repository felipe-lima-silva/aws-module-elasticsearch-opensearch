variable "project_name" {
  type        = string
  description = "(Required) Name of the project."
}

variable "cluster_name" {
  type        = string
  description = "(Required) This will be the name of the cluster."
}

variable "region" {
  type        = string
  description = "(Required) The region This is the region where the resources will be applied."
}

variable "environment_name" {
  type        = string
  description = "(Required) The name of the environment. must be `dev`, `hml`, `prd`."

  validation {
    condition     = contains(["dev", "hml", "prd"], var.environment_name)
    error_message = "Valid values for var: environment_name are (dev, hml, prd)."
  }
}

variable "master_user_name" {
  type        = string
  description = "(Required) Main user's username, which is stored in the Amazon Elasticsearch Service domain's internal database. Default `master`"
  default     = "master"
}

variable "vpc_id" {
  type        = string
  description = "(Required) Vpc id"
}

variable "subnet_ids" {
  type        = list(string)
  description = "(Required) List of subnet ids"

   validation {
     condition     = length(var.subnet_ids) >= 3
     error_message = "Subnet ids require minimum of 3 subnets."
   }

}

variable "is_test_mode" {
  type        = bool
  description = "(TEST MODE) use this option to avoid use the VPC based ES (OpenSearch)"
  default     = false
}

variable "dedicated_master_enabled" {
  type        = bool
  description = "(Optional) Whether dedicated main nodes are enabled for the cluster."

  default = false
}

variable "search_version" {
  type        = string
  description = "(Required) Version of Elasticsearch to deploy. Defaults is `OpenSearch_1.0.`" 
  default     = "OpenSearch_1.0"

  validation {
    condition     = contains( ["OpenSearch_1.0","7.10","7.9","7.8","7.7","7.4","7.1","6.8","6.7"], var.search_version)
    error_message = "Valid values for var: search_version are (OpenSearch_1.0, 7.10, 7.9, 7.8, 7.7, 7.4, 7.1, 6.8, 6.7) because the security package, see more detail in this [aws docuemntation](https://docs.aws.amazon.com/opensearch-service/latest/developerguide/supported-operations.html)."
  }
}

variable "instance_type" {
  type        = string
  description = " (Optional) Instance type of data nodes in the cluster. please check [aws documentation](https://docs.aws.amazon.com/opensearch-service/latest/developerguide/supported-instance-types.html) "
  default     = null
}

variable "tags" {
  type        = map(any)
  description = " (Optional) Map of tags to assign to the resource. If configured with a provider."
  default     = {}
}

variable "ebs_options_volume_size" {
  type        = number
  description = "(Optional) unless you select some instance that required the EBS volume."
  default     = 0
}

variable "ebs_options_volume_type" {
  type        = string
  description = "(Optional) Type of EBS volumes attached to data nodes. It will only be filled if the `ebs_options_volume_size` is not empty."
  default     = null
}

variable "ebs_options_iops" {
  type        = number
  description = "(Optional) Baseline input/output (I/O) performance of EBS volumes attached to data nodes. Applicable only for the Provisioned IOPS EBS volume type.  It will only be filled if the `ebs_options_volume_size` is not empty."
  default     = null
}

variable "password_rotate_after_days" {
  type        = number
  description = "(Required) Specifies the number of days between automatic scheduled rotations of the secret. Default 30 days"
  default     = 30
}

variable "password_length" {
  type        = number
  description = "(Required) Specifies the length of password. Default 32"
  default     = 32
}

variable "log_retention" {
  type        = number
  description = "(Optional) Specifies the number of days you want to retain log events in the specified log group. Possible values are: 1, 3, 5, 7, 14, 30, 60, 90, 120, 150, 180, 365, 400, 545, 731, 1827, 3653. If you select 0, the events in the log group are always retained and never expire."
  default     = 14
}

variable "rotate_lambda" {

  type = object({
    zip     = string
    handler = string
  })
  description = "(Required) Lambda responsable to rotate the password. Default zip = lambda_rotate.zip handler = main"
  default = {
    handler = "main"
    zip     = "lambda_rotate.zip"
  }
}
