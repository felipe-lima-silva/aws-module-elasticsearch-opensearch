resource "aws_elasticsearch_domain" "default" {

  domain_name           = var.cluster_name
  elasticsearch_version = var.search_version
  encrypt_at_rest {
    enabled    = true
    kms_key_id = aws_kms_key.default.id
  }

  cluster_config {
    instance_type = var.instance_type
    zone_awareness_enabled = true

    zone_awareness_config {
      availability_zone_count = 3
    }
  }

  advanced_security_options {
    enabled                        = true
    internal_user_database_enabled = true

    master_user_options {
      master_user_name     = var.master_user_name
      master_user_password = local.random_password
    }
  }

  node_to_node_encryption {
    enabled = true
  }

  domain_endpoint_options{
      enforce_https = true
      tls_security_policy = "Policy-Min-TLS-1-2-2019-07"
  }

  dynamic "ebs_options" {
    for_each = var.ebs_options_volume_size > 0 ? [1] : []

    content {
      ebs_enabled = true
      iops        = var.ebs_options_iops
      volume_size = var.ebs_options_volume_size
      volume_type = var.ebs_options_volume_type
    }
  }

  dynamic "vpc_options" {
    for_each = var.is_test_mode ? [] : [1]

    content {
      subnet_ids = var.subnet_ids

      security_group_ids = [aws_security_group.es.id]
    }
  }

  tags = var.tags

  depends_on = [aws_iam_service_linked_role.es]
}

resource "aws_iam_service_linked_role" "es" {
  aws_service_name = "es.amazonaws.com"
}

resource "aws_elasticsearch_domain_policy" "default" {
  count = var.is_test_mode ? 0 : 1

  domain_name = aws_elasticsearch_domain.default.domain_name

  access_policies = <<CONFIG
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "es:*",
            "Principal": "*",
            "Effect": "Allow",
            "Resource": "arn:aws:es:${var.region}:${data.aws_caller_identity.current.account_id}:domain/${var.cluster_name}/*"
        }
    ]
}
CONFIG
}

resource "aws_elasticsearch_domain_policy" "test_mode" {
  count = var.is_test_mode ? 1 : 0

  domain_name = aws_elasticsearch_domain.default.domain_name

  access_policies = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "es:*",
      "Principal": "*",
      "Effect": "Allow",
      "Resource": "arn:aws:es:${var.region}:${data.aws_caller_identity.current.account_id}:domain/${var.cluster_name}/*",
      "Condition": {
        "IpAddress": {"aws:SourceIp": ["${chomp(data.http.myip.body)}"]}
      }
    }
  ]
}
POLICY

}
