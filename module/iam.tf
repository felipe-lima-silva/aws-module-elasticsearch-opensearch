resource "aws_iam_role_policy_attachment" "lambda" {
  role       = aws_iam_role.rotate_lambda.name
  policy_arn = aws_iam_policy.rotate_policy.arn
}

resource "aws_iam_role" "rotate_lambda" {
  name               = "Role-Lambda-Rotate-${var.cluster_name}-${var.environment_name}"
  assume_role_policy = data.aws_iam_policy_document.assume_role.json
}

data "aws_iam_policy_document" "assume_role" {
  statement {
    sid    = "AssumeRoleAccess"
    effect = "Allow"

    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }

    actions = [
      "sts:AssumeRole",
    ]
  }
}

resource "aws_iam_policy" "rotate_policy" {
  name   = "Lambda-Policy-${var.cluster_name}-${var.environment_name}"
  policy = data.aws_iam_policy_document.lambda_rotate_policy.json
}


data "aws_iam_policy_document" "lambda_rotate_policy" {
  statement {
    sid    = "LogsAccess"
    effect = "Allow"

    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents"
    ]

    resources = [
      "arn:aws:logs:*:*:*",
    ]
  }

  statement {
    sid    = "SecretManagerAccess"
    effect = "Allow"

    actions = [
      "secretsmanager:DescribeSecret",
      "secretsmanager:GetSecretValue",
      "secretsmanager:PutSecretValue",
      "secretsmanager:UpdateSecretVersionStage",
    ]

    resources = [
      aws_secretsmanager_secret.default.arn,
    ]
  }

  statement {
    sid    = "AllowGenerateRandomPassword"
    effect = "Allow"

    actions = [
      "secretsmanager:GetRandomPassword",
    ]

    resources = ["*"]
  }

  statement {
      sid    = "SearchAccess"
      effect = "Allow"

      actions = [
        "es:*",
      ]

      resources = [
        aws_elasticsearch_domain.default.arn,
      ]
    }

  statement {
    sid    = "KmsAccess"
    effect = "Allow"

    actions = [
      "kms:DescribeKey",
    ]

    resources = [
      aws_kms_key.default.arn,
    ]
  }
}

data "aws_iam_policy_document" "es_policy" {
  statement {
    effect = "Allow"

    actions = [
      "es:*"
    ]

    resources = [
      "arn:aws:es:${var.region}:${data.aws_caller_identity.current.account_id}:domain/${var.cluster_name}/*",
    ]
  }
}

data "aws_iam_policy_document" "es_policy_test_mode" {
  statement {
    effect = "Allow"

    actions = [
      "es:*"
    ]

    resources = [
      "arn:aws:es:${var.region}:${data.aws_caller_identity.current.account_id}:domain/${var.cluster_name}/*",
    ]

    condition {
      test     = "IpAddress"
      variable = "aws:SourceIp"

      values = ["0.0.0.0/0"]
    }
  }
}


