resource "aws_kms_key" "default" {
  tags = var.tags
}

resource "aws_kms_alias" "default" {
  name          = "alias/${var.cluster_name}-${var.environment_name}"
  target_key_id = aws_kms_key.default.key_id
}