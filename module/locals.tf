locals {
  opensearch_verions = ["OpenSearch_1.0"]

  opensearch_service_name = "opensearch"

  random_password = random_password.password.result

  lambda_name = "Secret-Rotate-${var.cluster_name}-${var.project_name}-${var.environment_name}"
}

resource "random_password" "password" {
  length  = var.password_length
  special = true
}

resource "random_id" "server" {
  byte_length = 8
}

data "http" "myip" {
  url = "http://ipv4.icanhazip.com"
}
