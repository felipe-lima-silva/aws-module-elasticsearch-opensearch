data "aws_vpc" "selected" {
  id = var.vpc_id
}

resource "aws_security_group" "es" {
  name        = "SG-${var.cluster_name}-${var.environment_name}"
  description = "Managed by Terraform"
  vpc_id      = var.vpc_id

  ingress {
    from_port = 443
    to_port   = 443
    protocol  = "tcp"

    cidr_blocks = [
      data.aws_vpc.selected.cidr_block,
    ]
  }

  tags = var.tags
}